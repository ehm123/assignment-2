 using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2
{
    class Program
    {
        static void Main(string[] args)
        {
            //run game
            Game g = new Game();
            g.Run();
        }
    }

    public class Game
    {

        public void Run()
        {

            // starting the game
            Console.WriteLine("Starting Game!");
                

            Board board = new Board();

            bool runningGame = true;

            // moves character
            while (runningGame)
            {
                Console.WriteLine("");
                Console.WriteLine("Use W, A, S, D to move and type 'exit' to quit the game");
                Console.WriteLine("Type 'map' to see the current map");
                Console.WriteLine("");
                Console.WriteLine("Player Stats:");
                Console.WriteLine("Lives: " + board.PlayerLives());
                Console.WriteLine("Treasure: " + board.PlayerTreasure());
                Console.WriteLine("");

                //get input from the user
                string input = Console.ReadLine();
                    
                //move north
                if (input.Equals("w"))
                {
                    runningGame = !board.DirectionMoved("w");

                    // move west	
                }
                else if (input.Equals("a"))
                {
                    runningGame = !board.DirectionMoved("a");

                    // move east
                }
                else if (input.Equals("d"))
                {
                    runningGame = !board.DirectionMoved("d");

                    //move south
                }
                else if (input.Equals("s"))
                {
                    runningGame = !board.DirectionMoved("s");


                    //exit game
                }
                else if (input.Equals("exit"))
                {
                    Console.WriteLine("Exit Game");
                    runningGame = false;
                } 
                else if (input.Equals("map"))
                {
                    Console.WriteLine("Printing Map");
                    board.PrintBoard();
                } else if (input.Equals("t"))
                {
                    Console.WriteLine("Collecting treaure");
                    if (board.FindSqaure("t"))
                    {
                        board.CollectTreasure();
                        Console.WriteLine("Collected treaure");
                    }
                    else
                    {
                        Console.WriteLine("There is no treasure to collect");

                    }
                }
                else
                {
                    Console.WriteLine("Invalid input. Please try again");
                }

                   
                    
            }

            Console.WriteLine("Game Over. Exiting Game. Press anything to exit");
            Console.ReadLine();

        }



    }

    public class Board
    {

        private int boardSize = 6;
        private string[,] board;

        private Random random = new Random();

        //this keeps track of the player, treasure, health etc.
        private Player player = new Player();

        private string start = "s";
        private string exit = "e";
        private string treasure = "t";
        private string hazard = "h";

        private List<string> previousLocations = new List<string>();




        public Board()
        {
            board = new string[boardSize, boardSize];
            for(int i = 0; i < boardSize; i++)
            {
                for (int j = 0; j < boardSize; j++)
                {
                board[i, j] = "";
                }
            }
            CreateBoard();
        

        }


        //tries to move the player in the direction
        public bool DirectionMoved(string direction)
        {
        

        bool gameOver = false;

            //decides what square to move the player onto
            if (direction.Equals("w"))
            {
                   
                gameOver = MovePlayer(player.GetRow() -1 , player.GetCol() );

                // move west	
            }
            else if (direction.Equals("a"))
            {
                gameOver= MovePlayer(player.GetRow(), player.GetCol() -1);


                // move east
            }
            else if (direction.Equals("d"))
            {
                gameOver= MovePlayer(player.GetRow(), player.GetCol()+1 );


                //move south
            }
            else if (direction.Equals("s"))
            {
                gameOver= MovePlayer(player.GetRow()+1 , player.GetCol());


            }
            return gameOver;
        }

        //move the player on the board
        public bool MovePlayer(int row, int col)
        {
            bool gameOver = false;

            //moves the player
            //first checks if the player is trying to move into a correct sqaure location
            if (row < boardSize && col < boardSize && row >= 0 && col >= 0)
            {
                previousLocations.Add("(" + player.GetRow() + ", " + player.GetCol() + ")");
                player.SetRow(row);
                player.SetCol(col);

                

                Console.WriteLine("Player moved");
                Console.WriteLine();
                
                //if treasure is in a sqaure next to them
                if (FindSqaure(treasure))
                {
                    Console.WriteLine("Treasure is in a neighbouring square. Press T to collect");
                }

                //if hazards is around
                if (FindSqaure(hazard))
                {
                    Console.WriteLine("Hazard is in a neighbouring square");
                }

            
       
                //checks to see if the sqaure its landed on has a special proptery

                // if player lands of exit sqaure
                if (board[row, col].Equals(exit))
                {

                    Console.WriteLine("You have found the end of the maze!");
                    Console.WriteLine("Exiting Game");
                    gameOver = true;

                    
                }

                // if player lands on hazard sqaure
                else if (board[row, col].Equals(hazard))
                {
                    
                    Console.WriteLine("You step on a hazard");
                    Console.WriteLine("You lost one of your lives");
                    player.DecreaseLives();

                    if (player.IsDead())
                    {
                        Console.WriteLine("You died!");
                        Console.WriteLine("Exiting Game");
                        gameOver = true;
                    }


                }

                // if player lands on treasure sqaure, they coect treasure
                else if (board[row, col].Equals(treasure))
                {
                    Console.WriteLine("You found teasure");
                    player.IncreaseTreasure();
                    RemoveTreasure(player.GetRow(), player.GetCol());

                }
                else
                {
                    

                }
            }
            else
            {
                Console.WriteLine("Player  can't move in that direction");
            }

                return gameOver;

        }
            
        public int PlayerLives()
        {
            return player.GetLives();
        }

        public int PlayerTreasure()
        {
            return player.GetTreasure();
        }

        public void PrintPlayerLocation()
        {
            Console.WriteLine("Player location: (" + player.GetRow() + ", " + player.GetCol()+")");
        }

        public void RemoveTreasure(int row, int col)
        {
            board[row, col] = "";
        }

        //adds special squares to board such as start, exit etc.
        public int[] AddToBoard(string sqaure, int amount)
        {

            int counter = 0;
            int row = 0;
            int col = 0;

            while (counter < amount)
            {

                //gets random row and col sqaures
                row = random.Next(0, boardSize);
                col = random.Next(0, boardSize);

                // if there is nothing currently in that sqaure
                //then add the special square
                if (board[row, col].Equals(""))
                {
                    board[row, col] = sqaure;
                    counter++;
                }

            }

            //returns the last location of the last special sqaure
            return new int[] { row, col };
        }

        public bool CollectTreasure()
        {
            //get current square of the player on the board
            int col = player.GetCol();
            int row = player.GetRow();


            bool treasureFound = false;

            //checks all sqaures around player

            //top left sqaure
            if (col - 1 >= 0 && row - 1 >= 0 && board[row - 1, col - 1] == treasure)
            {
                treasureFound = true;
                player.IncreaseTreasure();
                RemoveTreasure(row - 1, col - 1);
             

            }

            // top
            if (row - 1 >= 0 && board[row - 1, col] == treasure)
            {
                treasureFound = true;
                player.IncreaseTreasure();
                RemoveTreasure(row - 1, col);
              
            }

            //left
            if (col - 1 >= 0 && board[row, col - 1] == treasure)
            {
                treasureFound = true;
                player.IncreaseTreasure();
                RemoveTreasure(row, col - 1);
             
            }

            //bottom
            if (row + 1 < boardSize && board[row + 1, col] == treasure)
            {
                treasureFound = true;
                player.IncreaseTreasure();
                RemoveTreasure(row + 1, col);
             
            }

            //right
            if (col + 1 < boardSize && board[row, col + 1] == treasure)
            {
                treasureFound = true;
                player.IncreaseTreasure();
                RemoveTreasure(row, col + 1);
             
            }

            //bottom right
            if (col + 1 < boardSize && row + 1 < boardSize && board[row + 1, col + 1] == treasure)
            {
                treasureFound = true;
                player.IncreaseTreasure();
                RemoveTreasure(row + 1, col + 1);
             
            }

            //bottom left
            if (col - 1 >= 0 && row + 1 < boardSize && board[row + 1, col - 1] == treasure)
            {
                treasureFound = true;
                player.IncreaseTreasure();
                RemoveTreasure(row + 1, col - 1);
             
            }

            //top right
            if (col + 1 < boardSize && row - 1 >= 0 && board[row - 1, col + 1] == treasure)
            {
                treasureFound = true;
                player.IncreaseTreasure();
                RemoveTreasure(row - 1, col + 1);
               
            }

            return treasureFound;

        }

        //prints board to screen
        public void PrintBoard()
        {
            Console.Write("|   ");
            for (int h=0; h<boardSize; h++)
            {
                Console.Write("| " + h+" ");
            }
            Console.WriteLine("");
           

            for (int i = 0; i < boardSize; i++)
            {
                Console.Write("| "+i+" ");
                for (int j = 0; j < boardSize; j++)
                {
                    if (board[i, j].Equals(""))
                    {
                        Console.Write("|   ");
                    }
                    else
                    {
                        Console.Write("| " + board[i, j] + " ");
                    }
                    
                }
                Console.WriteLine("");
                
            }
            PrintPlayerLocation();


            Console.Write("Previous Locations: ");
            foreach (string location in previousLocations)
            {
                Console.Write(location+ ", ");
            }
            Console.WriteLine("");
            Console.WriteLine("");
        }

        //creates board and its special squares
        public void CreateBoard()
        {
            //add  special squares to board

            //number of special suares to add to board
            int numStart = 1;
            int numEnd = 1;
            int numTreasures = 3;
            int numHazards = 5;

            //adds a start position to board and gets the position
            int[] playerPoisiton = AddToBoard(start, numStart);
            
            //set start position to player position
            player.SetRow(playerPoisiton[0]);
            player.SetCol(playerPoisiton[1]);

            
           

            AddToBoard(exit, numEnd);
            AddToBoard(treasure, numTreasures);
            AddToBoard(hazard,numHazards);

        
        }


            // finds if a special square is neighbouring another square
        public bool FindSqaure(string sqaure)
        {

                //get current square of the player on the board
            int col = player.GetCol();
            int row = player.GetRow();


            bool squareFound = false;

            //checks all sqaures around player

                //top left sqaure
            if (col-1>=0 && row-1>=0 && board[row - 1, col - 1 ] == sqaure)
            {
                squareFound = true;
                

                }

            // top
            if (row-1 >=0 && board[row - 1, col ] == sqaure)
            {
                squareFound = true;
                 
                }

            //left
            if (col-1 >= 0 && board[row, col - 1  ] == sqaure)
            {
                squareFound = true;
               
                }

            //bottom
            if (row +1 < boardSize && board[row + 1, col ] == sqaure)
            {
                squareFound = true;
                
                }

            //right
            if (col+1<boardSize && board[ row, col + 1] == sqaure)
            {
                squareFound = true;
                 
                }

            //bottom right
            if (col+1 < boardSize && row+1<boardSize && board[row + 1, col + 1 ] == sqaure)
            {
                squareFound = true;
                 
                }

            //bottom left
            if (col-1 >= 0 && row+1<boardSize && board[row + 1, col - 1 ] == sqaure)
            {
                squareFound = true;
                
            }

            //top right
            if (col+1 < boardSize && row-1 >=0 && board[row - 1, col + 1 ] == sqaure)
            {
                squareFound = true;
                 
                }

            return squareFound;
        }

    }

    public class Player
    {

        // row and column on the player on the board
        private int row = 0;
        private int col = 0;
        private int treasure = 0;
        private int lives = 3 ;

        public Player()
        {

        }

        public int GetRow()
        {
            return row;
        }

        public int GetCol()
        {
            return col;
        }

        public void SetCol(int newCol)
        {
            col = newCol;

        }
        public void SetRow(int newRow)
        {
            row = newRow;
        }

        public void IncreaseTreasure()
        {
            treasure++;
        }
            
        public void DecreaseLives()
        {
            lives--;
        }

        public void IncreaseLives()
    {
        lives++;
    }

        public bool IsDead()
        {
            if (lives <= 0)
            {
                    return true;
            }
                    return false;
        }

    public int GetLives()
    {
        return lives;
    }
            
    public int GetTreasure()
    {
        return treasure;
    }

        public void PrintLocation()
        {
            Console.WriteLine("Player row: " + row + " col: " + col);
        }

    }
    

}
